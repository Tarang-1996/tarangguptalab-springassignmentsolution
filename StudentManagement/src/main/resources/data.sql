-- Insert data into role table

INSERT INTO role VALUES(1, 'ADMIN'); 
INSERT INTO role VALUES(2, 'USER');

-- Insert data into usertable

INSERT INTO user VALUES(1, '$2a$12$2f1hTtrsKlC.nrjbKW6XoOTY0klV5A/.2drrzBDCV4NpwOnGiiFbK', 'Tarang'); 
INSERT INTO user VALUES(2, '$2a$12$YnxRIQkxO9ISCZ5RlD8rS.4es6XjMdA7drp7hPMUvUCdGwzwoWQAS', 'Admin');

--Insert data into users_roles table
INSERT INTO users_roles VALUES(1, 2);
INSERT INTO users_roles VALUES(2, 1);