<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<html>
<body>
	<div class="container-fluid p-3   text-center ">
			<h2>${msg}</h2>
			<h3>Click here to go back to login page <a href="/StudentManagement/login"
			class="btn btn-info btn-sm mb-3 float-right"> Login </a></h3>
	</div>
	
</body>
</html>