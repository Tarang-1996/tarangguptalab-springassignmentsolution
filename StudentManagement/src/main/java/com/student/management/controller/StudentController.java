package com.student.management.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.student.management.model.Student;
import com.student.management.service.StudentService;

/**
 * @author Tarang Gupta
 *
 */

@Controller
@RequestMapping("/student")
public class StudentController {

	@Autowired
	StudentService studentService;

	@RequestMapping("/list")
	public String listStudent(Model model) {

		List<Student> students = studentService.findAllStudent();

		model.addAttribute("Students", students);

		return "list-Students";

	}

	@RequestMapping("/showFormForAdd")
	public String showFormForAdd(Model model) {

		Student student = new Student();

		model.addAttribute("Student", student);

		return "student-form";

	}

	@RequestMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("studentId") int id, Model model) {

		Student student = studentService.findById(id).get();

		model.addAttribute("Student", student);

		return "student-form";

	}

	@RequestMapping("/save")
	public String save(@RequestParam("id") int id, @RequestParam("firstName") String firstName,
			@RequestParam("lastName") String lastName, @RequestParam("country") String country,
			@RequestParam("course") String course) {

		Student student;

		if (id != 0) {
			student = studentService.findById(id).get();
			student.setFirstName(firstName);
			student.setLastName(lastName);
			student.setCountry(country);
			student.setCourse(course);
		} else {

			student = Student.builder().firstName(firstName).lastName(lastName).country(country).course(course).build();
		}

		studentService.saveStudent(student);

		return "redirect:/student/list";
	}

	@RequestMapping("/delete")
	public String delete(@RequestParam("studentId") int id) {

		studentService.deleteStudent(id);

		return "redirect:/student/list";

	}

	@RequestMapping(value = "403")
	public ModelAndView accessDenied(Principal principal) {
		ModelAndView modelAndView = new ModelAndView();

		if (principal != null) {

			modelAndView.addObject("msg",
					"Hello " + principal.getName() + " You dont have permission to access this page");

		} else {

			modelAndView.addObject("msg", "You don't have permission to access this page");

		}

		modelAndView.setViewName("403");
		return modelAndView;

	}

}
