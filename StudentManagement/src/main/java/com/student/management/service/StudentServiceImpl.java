/**
 * 
 */
package com.student.management.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.student.management.dao.StudentRepository;
import com.student.management.model.Student;

/**
 * to perform business logic for student
 * 
 * @author Tarang Gupta
 *
 */
@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	StudentRepository studentRepo;

	@Override
	public List<Student> findAllStudent() {

		return studentRepo.findAll();
	}

	@Override
	public Optional<Student> findById(int id) {
		
		return studentRepo.findById(id);
	}

	@Override
	public Student saveStudent(Student student) {
		
		return studentRepo.save(student);
	}

	@Override
	public Student updateStudent(Student student) {
		
		return studentRepo.save(student);
	}

	@Override
	public void  deleteStudent(int id) {
		
		studentRepo.deleteById(id);
	}

}
