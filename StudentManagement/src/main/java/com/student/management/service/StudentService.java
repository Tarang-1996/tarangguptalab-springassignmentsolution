package com.student.management.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.student.management.model.Student;

/**
 * 
 * @author Tarang Gupta
 *
 */
@Service
public interface StudentService {

	List<Student> findAllStudent();

	Optional<Student> findById(int id);

	Student saveStudent(Student student);

	Student updateStudent(Student student);

	void deleteStudent(int id);

}