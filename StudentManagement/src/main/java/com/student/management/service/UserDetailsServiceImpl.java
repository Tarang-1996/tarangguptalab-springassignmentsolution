package com.student.management.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.student.management.dao.UserRepository;
import com.student.management.model.User;
import com.student.management.security.MyUserDetails;

/**
 * to perform business logic for user
 * 
 * @author Tarang Gupta
 *
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UserRepository userRepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepo.getUserByUserName(username);

		if (user == null) {
			throw new UsernameNotFoundException("Given user not found");
		}

		return new MyUserDetails(user);
	}

}
