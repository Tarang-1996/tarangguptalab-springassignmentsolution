package com.student.management.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.student.management.model.Student;
/**
 * to perform data base operation for Student bean
 * 
 * @author Tarang Gupta
 */
@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {

}
