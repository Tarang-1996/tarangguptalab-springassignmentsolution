package com.student.management.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.student.management.model.User;

/**
 * to perform data base operations for User Bean
 * 
 * @author Tarang Gupta
 *
 */
@Repository
public interface UserRepository  extends JpaRepository<User, Integer>{
	
	@Query("SELECT u FROM User u WHERE u.username= ?1")
	public User getUserByUserName(String username);

}
