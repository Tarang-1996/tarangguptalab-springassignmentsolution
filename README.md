# TarangGuptaLab-SpringAssignmentSolution

This project is developed using spring boot, spring security, jsp and mysql

## Getting started

To run the project on the local system, Kindly follow below steps:

1- Create database name-"student_management"

2- Update the mysql username and password in resources/application.properties file

3- There is some initial data mentioned in data.sql file. If there is any error in executing that script than application will fail

4- There are two user created in application 

    UserName    Password    Role
    Tarang      Tarang@123  USER
    Admin       Admin@123   ADMIN

5- Refer the screenshots and video for more help





